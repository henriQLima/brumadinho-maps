// Initializing Application
console.log('loaded app.js - SOS Brumadinho')

// Declaring the variables used on the functions for app.js
var map,
  Lotes,
  LotesData,
  LotesCenter,
  LotesSearch,
  roadsData,
  roadsTopojson,
  currentStyle,
  newStyle,
  searchResultCurrent,
  searchResultId,
  searchsearchResultString,
  LotesCentroids,
  popup,
  popup2,
  mapBasemaps = [],
  mapLayers = [],
  flying = false,
  baseLayerChange = false,
  searchFound = false,
  gmapsorig,
  userLoc = 0,
  tries = 0;

var options = {
}

var token = 'pk.eyJ1IjoiaGVucmlxbGltYSIsImEiOiI2NGVhNTllNTNjOWVmZGVlMDhiOGYzMWQ4MmE4NGY2OSJ9.TfjCYyxPQ_gNcd1JEL7C2w';

/*
* TESTING ON LOADING ALL THE GEOJSON DATA BEGORE BUILDING THE MAP APPLICATION
*/
// $.ajax({
//   type: "GET",
//   crossDomain: true,
//   dataType: 'jsonp',
//   headers: {"Access-Control-Allow-Origin": "https://www.dropbox.com/s/q4rplga2itryqqo/Status_Lotes_JDF-02-10.geojson?dl=0"},
//   url: "https://www.dropbox.com/s/q4rplga2itryqqo/Status_Lotes_JDF-02-10.geojson?dl=0"
// }).done(function (data) {
//   console.log('minha data', data);
// });


//Loading main GEOJSON into the application
$.when(
  $.getJSON('https://dl.dropboxusercontent.com/s/6stmwq834emeyny/brumadinho_edificacoes.geojson?dl=0', function (loadeddata1) {
    LotesData = loadeddata1;
  })
).then(function () {
  mapInit();
});

/*
* Map functions
*/

function mapInit(div, opts, key) {
  console.log('building map');

  //Getting a centroid from each parcel on the map to point to the route on google maps
  //LotesCenter = turf.center(LotesData);

  mapboxgl.accessToken = token;

  mapBasemaps.streets = 'mapbox://styles/mapbox/streets-v9';
  currentStyle = 'satellite-streets-v9';
  mapBasemaps.satellite = 'mapbox://styles/mapbox/satellite-streets-v9';

  addMapBasemapsToggle('basemapSwitcher', 'streets-v9', 'Streets', false, 1);
  addMapBasemapsToggle('basemapSwitcher', 'satellite-streets-v9', 'Satellite', true, 2);

  map = new mapboxgl.Map({
    container: 'map',
    hash: true,
    style: mapBasemaps.satellite,
    center: [-44.120837, -20.123083],
    zoom: 15.3,
    maxZoom: 23,
    //bearing: 10,
    //pitch: 55,
    preserveDrawingBuffer: true
  });
  index = 0

  var draw = new MapboxDraw({
    displayControlsDefault: false,
    controls: {
      line_string: true,
      trash: true
    }
  });
  map.addControl(draw, 'top-left');

  map.on('draw.create', updateArea);
  map.on('draw.delete', updateArea);
  map.on('draw.update', updateArea);

  function updateArea(e) {
    var data = draw.getAll();
    //console.log('DATA', data)
    //console.log('find layer name draw', draw)
    var answer = document.getElementById('calculated-area');
    if (data.features.length > 0) {
    var line = turf.lineString(data.features[0].geometry.coordinates);
    var area = turf.length(line, {units: 'kilometers'});
    // restrict to area to 2 decimal points
    var rounded_area = Math.round(area*100)/100;
    answer.innerHTML = '<p><strong>' + rounded_area + '</strong> Km</p>';
    } else {
    answer.innerHTML = '';
    if (e.type !== 'draw.delete') alert("use a ferramenta para medir a distancia!");
    }
    }

  map.addControl(new mapboxgl.NavigationControl());
  var gpscontrol = new mapboxgl.GeolocateControl({
    positionOptions: {
      enableHighAccuracy: true
    },
    trackUserLocation: true
  });

  map.addControl(gpscontrol);
  gpscontrol.on('geolocate', function (e) {
    userLoc = 1;
    gmapsorig = e.coords.latitude + "," + e.coords.longitude;
  });

  map.addControl(new mapboxgl.FullscreenControl());

  //TESTING ROUTE TOOLS DIRECT FROM MAPBOX API 
  /*  var mbrouting = new MapboxDirections({
      accessToken: token
    });
    map.addControl(mbrouting, 'top-left')
  
    $("#mboxrouting").on('click', function() {
      if($(".mapboxgl-ctrl-directions").length === 0) {
        map.addControl(mbrouting, 'top-left')
      }else{
        map.removeControl(mbrouting);
        map.removeSource('directions');
      }
    })*/

  //POPUP CONFIG
  popup = new mapboxgl.Popup();
  popup2 = new mapboxgl.Popup();

  popup.on('close', function () {
    if (typeof map.getLayer('selected') != "undefined") {
      map.removeLayer('selected')
      //map.removeSource('selected');
    }
  });
  map.on('load', function () {
    addSources();
    searchInit();
  });
}

//Functions that links the menu and checkbox with the map parcel features
//1.
function addMapLayersToggle(id, layer, name, on) {
  if (!document.getElementById(layer)) {
    var label = document.createElement('label');
    var input = document.createElement('input');
    input.id = layer;
    input.type = 'checkbox';
    input.className = 'mdl-checkbox__input';
    var span = document.createElement('span');
    var spanText = document.createTextNode(name);
    span.appendChild(spanText);
    span.className = 'mdl-checkbox__label';
    label.appendChild(span);
    label.appendChild(input);
    label.className = 'mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect check';
    label.for = layer;
    label.id = layer + 'label';
    componentHandler.upgradeElement(label);
    document.getElementById(id).appendChild(label);
    if (on === true) {
      document.getElementById(layer + 'label').MaterialCheckbox.check();
    }
  }
}
//2.
function addMapBasemapsToggle(id, layer, name, on, val) {
  if (!document.getElementById(layer)) {
    var div = document.createElement('div');
    var label = document.createElement('label');
    var input = document.createElement('input');
    input.id = layer;
    input.type = 'radio';
    input.className = 'mdl-radio__button';
    input.name = "options";
    input.value = val;
    var span = document.createElement('span');
    var spanText = document.createTextNode(name);
    span.appendChild(spanText);
    span.className = 'mdl-checkbox__label';
    label.appendChild(input);
    label.appendChild(span);
    label.id = layer + 'label';
    label.className = 'mdl-radio mdl-js-radio mdl-js-ripple-effect';
    label.for = layer;
    componentHandler.upgradeElement(label);
    div.appendChild(label);
    document.getElementById(id).appendChild(div);
    if (on === true) {
      document.getElementById(layer + 'label').MaterialRadio.check();
    }
  }
}

//Function that adds the parcels(geojson) to the map as a layer and individual features
function addSources() {
  if (LotesData.features) {
    console.log('adding sources');

    /*check for sources first in map*/

    if (!map.getSource('LotesSource')) {
      map.addSource('LotesSource', {
        'type': 'geojson',
        'data': LotesData
      });
      map.addSource('18Jan', {
        type: 'raster',
        url: 'mapbox://henriqlima.1idh3c4o'
      });
      map.addSource('27Jan', {
        type: 'raster',
        url: 'mapbox://henriqlima.au0m5q37'
      });
      map.addSource('28Jan', {
        type: 'raster',
        url: 'mapbox://henriqlima.33munnh3'
      });
      map.addSource('29Jan', {
        type: 'raster',
        url: 'mapbox://henriqlima.2c7a9bs2'
      });
      map.addSource('After', {
        type: 'raster',
        url: 'mapbox://henriqlima.8gxo2he7'
      });
      map.addSource('Before', {
        type: 'raster',
        url: 'mapbox://henriqlima.44r4htn1'
      });
      map.addSource('infraBefore', {
        type: 'raster',
        url: 'mapbox://henriqlima.0ggouuof'
      });
      map.addSource('infraAfter', {
        type: 'raster',
        url: 'mapbox://henriqlima.5nkmvx1l'
      });
      map.addSource('AutoCad', {
        'type': 'geojson',
        'data': 'https://dl.dropboxusercontent.com/s/bqkex90cxmz3s0p/brumadinho_vetorizacao_area_afetada.geojson?dl=0'
      });
    }
    addMapLayers()
  } else {
    setTimeout(addSources, 200)
  }
}

function addMapLayers() {
  console.log('adding layers');

  if (!mapLayers.Lotes) {
    ///////MY LAYERS///////
    map.addLayer({
      "id": "Ortho18Jan",
      "type": "raster",
      "source": "18Jan",
      'layout': {
        'visibility': 'none'
      }
    }, 'gl-draw-line-active.cold');
    map.addLayer({
      "id": "Ortho27Jan",
      "type": "raster",
      "source": "27Jan",
      'layout': {
        'visibility': 'none'
      }
    }, 'gl-draw-line-active.cold');
    map.addLayer({
      "id": "Ortho28Jan",
      "type": "raster",
      "source": "28Jan",
      'layout': {
        'visibility': 'none'
      }
    }, 'gl-draw-line-active.cold');
    map.addLayer({
      "id": "Ortho29Jan",
      "type": "raster",
      "source": "29Jan"
    }, 'gl-draw-line-active.cold');
    map.addLayer({
      "id": "OrthoBefore",
      "type": "raster",
      "source": "Before",
      'layout': {
        'visibility': 'none'
      }
    });
    map.addLayer({
      "id": "OrthoAfter",
      "type": "raster",
      "source": "After",
      'layout': {
        'visibility': 'none'
      }
    });
    map.addLayer({
      "id": "infraBeforeb",
      "type": "raster",
      "source": "infraBefore",
      'layout': {
        'visibility': 'none'
      }
    });
    map.addLayer({
      "id": "infraAfterb",
      "type": "raster",
      "source": "infraAfter",
      'layout': {
        'visibility': 'none'
      }
    });
    /////////////////////////
    mapLayers.Lotes = {
      'id': 'Lotes',
      'type': 'fill',
      'source': 'LotesSource',
      'paint': {
        //'fill-color': 'rgba(0,191,255,0.5)',
        'fill-color': ['match', ['get', 'Situacao'], //
          'vendido', 'rgba(220,0,0,0.7)', // 
          "disponivel", 'rgba(0,226,44,0.7)', //
          "indisponivel", 'rgba(0,0,0, 0.7)', //
          "reservado socio", 'rgba(255,255,0,0.7)', // ...
          'rgba(229,229,229, 0.1)'],  // white otherwise
        "fill-outline-color": "slategray"

      },
      'filter': ['!=', 'Lote', ''],
      'layout': {
        'visibility': 'visible'
      }
    };

    mapLayers.LotesLines = {
      'id': 'LotesLines',
      'type': 'line',
      'source': 'LotesSource',
      'paint': {
        'line-width': 1.5,
        "line-color": "white"
      },
      'layout': {
        'visibility': 'visible'
      }
    };

    mapLayers.AreaLine = {
      'id': 'AreaLine',
      'type': 'line',
      'source': 'AutoCad',
      'paint': {
        'line-width': 1.5,
        "line-color": "red"
      },
      'layout': {
        'visibility': 'visible'
      }
    };

    addMapLayersToggle("mapLayerSwitcher", "Lotes", "Edificações", true, 1);
    addMapLayersToggle("mapLayerSwitcher", "AreaLine", "Area impactada", true, 2);

    addMapLayersToggle("mapJpegSwitcher", "OrthoBefore", "barragem antes", false, 1);
    addMapLayersToggle("mapJpegSwitcher", "OrthoAfter", "barragem depois", false, 2);
    addMapLayersToggle("mapJpegSwitcher", "infraBeforeb", "infraestrutura antes", false, 3);
    addMapLayersToggle("mapJpegSwitcher", "infraAfterb", "infraestrutura depois", false, 4);

    addMapLayersToggle("mapOrtoSwitcher", "Ortho18Jan", "18 de Janeiro", false, 1);
    addMapLayersToggle("mapOrtoSwitcher", "Ortho27Jan", "27 de Janeiro", false, 2);
    addMapLayersToggle("mapOrtoSwitcher", "Ortho28Jan", "28 de Janeiro", false, 3);
    addMapLayersToggle("mapOrtoSwitcher", "Ortho29Jan", "29 de Janeiro", true, 4);

  }

  /*
  * add visibility check when changing map styles
  */

  //WORKING ON CODE COMMENTS/ORGANIZING BELOW THIS POINT BECAUSE A COUPLE TESTS ARE IN PROGRESS...

  for (layer in mapLayers) {
    //console.log(layer);
    //console.log(mapLayers[layer]);
    if (layer === 'Ortho') {
      currentStyle == 'streets-v9' ? map.addLayer(mapLayers[layer], "national_park") :
        map.addLayer(mapLayers[layer], "road-street")
    } else {
      map.addLayer(mapLayers[layer])
    }
  }

  map.on('mouseenter', 'Lotes', function () {
    map.getCanvas().style.cursor = 'pointer';
  });
  map.on('mouseleave', 'Lotes', function () {
    map.getCanvas().style.cursor = '';
  });

  if (baseLayerChange === false) {
    addListeners()
  }
}

function addListeners() {
  //console.log('adding event listeners');

  $("#mapLayerSwitcher").on('change', '.mdl-checkbox', function (e) {
    var layer = e.target.id;
    //console.log(layer, "clicked");
    /* switch layers by changing visibility*/
    if (layer != 'checkbox-0') {
      map.getLayoutProperty(layer, 'visibility') === 'none' ?
        map.setLayoutProperty(layer, 'visibility', 'visible') :
        map.setLayoutProperty(layer, 'visibility', 'none');
    }
    map.getLayoutProperty('Lotes', 'visibility') === 'none' ?
      map.setLayoutProperty('LotesLines', 'visibility', 'none') :
      map.setLayoutProperty('LotesLines', 'visibility', 'visible');
    mapLayers.Lotes.layout.visibility = map.getLayoutProperty('Lotes', 'visibility');
    mapLayers.LotesLines.layout.visibility = map.getLayoutProperty('Lotes', 'visibility');
    //console.log(mapLayers.AreaLine, "---maplayers")
    mapLayers.AreaLine.layout.visibility = map.getLayoutProperty('AreaLine', 'visibility');
  });

  $("#mapOrtoSwitcher").on('change', '.mdl-checkbox', function (e) {
    var layer = e.target.id;
    //console.log(layer);
    /* switch layers by changing visibility*/
    if (layer != 'checkbox-0') {
      map.getLayoutProperty(layer, 'visibility') === 'none' ?
        map.setLayoutProperty(layer, 'visibility', 'visible') :
        map.setLayoutProperty(layer, 'visibility', 'none');
    }
  });

  $("#mapJpegSwitcher").on('change', '.mdl-checkbox', function (e) {
    var layer = e.target.id;
    //console.log(layer);
    /* switch layers by changing visibility*/
    if (layer != 'checkbox-0') {
      map.getLayoutProperty(layer, 'visibility') === 'none' ?
        map.setLayoutProperty(layer, 'visibility', 'visible') :
        map.setLayoutProperty(layer, 'visibility', 'none');
    }
  });

  $(".mdl-radio").change(function () {
    basemapSwitch(this);
  });


  function basemapSwitch(layer) {
    currentStyle = [map.getStyle()];
    var str = currentStyle[0].name;
    str = str.replace(/\s+/g, '-').toLowerCase();
    //console.log("current style: " + str);
    newStyle = ($(layer).children()[0].id).toString();
    //var newStyle = layer.target.id;
    //console.log("new style: " + newStyle);
    //check if the current style is the same as the clicked style - this means that the style name in the json must match the id of the clicked layer in the material-mapbox template
    if (str != newStyle) {
      //newStyle == 'streets-v9'? mapLayers.Roads.paint["line-color"] = 'black' : mapLayers.Roads.paint["line-color"] = 'white';
      baseLayerChange = true;
      map.setStyle('mapbox://styles/mapbox/' + newStyle);
    }
  }


  map.on('sourcedataloading', function (e) {
    flying = true;
    //console.log('loading');
    var checkLoaded = setInterval(loaded, 100);

    function loaded() {
      if (map.getLayer('Lotes')) {
        //console.log('tiles loaded: ', map.areTilesLoaded());
      }
      if (map.areTilesLoaded()) {
        window.clearInterval(checkLoaded);
        //console.log('all tiles are loaded', ' flying: ', flying);
        flying = false;
        map.off('sourcedataloading');
      }
    }
  });

  map.on('click', function (e) {
    searchFound = false;
    if (typeof map.getLayer('selected') != "undefined") {
      map.removeLayer('selected')
      // map.removeSource('selected');
    }
    getInfo(e)
  });

  //function para distancia

  document.getElementById('map-loading').style.display = 'none';

}

/*
* global getinfo function
*/

function getInfo(e, uid, sr) {
  //console.log(uid, sr);
  if (flying === false) {
    if (sr === true) {
      var pt = map.project(e);
      var bbw = 0.001;
      var bbh = 0.001;
      /*var features = map.queryRenderedFeatures(
        [pt.x - bbw, pt.y - bbh],
        [pt.x + bbw, pt.y + bbh], {layers:['Lotes'], filter:['==', 'CPF do Cadastrante', uid]});*/
      var features = map.querySourceFeatures("LotesSource", { sourceLayer: ["Lotes"], filter: ["==", "Lote", uid] });
    } else {
      var features = map.queryRenderedFeatures(e.point, { layers: ['Lotes'] });
    }

    if (!features.length) {
      if (searchFound === true && tries < 5) {
        setTimeout(function () {
          getInfo(e, uid, sr)
        }, 1000);
      }
      if (searchFound === true && tries === 5) {
        searchFound = false;
        //alert('No features found! If this is unexpected, something went wrong. Please try again.')
      }
      tries = tries + 1;
      return false;
    }

    if (typeof map.getLayer('selected') != "undefined") {
      map.removeLayer('selected')
      //map.removeSource('selected');
    }

    //  var uidarray = 

    var feature = features[0];
    //console.log(feature)
    var index = (feature.properties);
    var uid2 = index["Lote"];

    var popuploc2 = turf.center(feature);
    //console.log(popuploc2.geometry);
    var gmapsloc = popuploc2.geometry.coordinates[1] + "," + popuploc2.geometry.coordinates[0];
    if (userLoc === 0) {
      gmapsorig = "-22.062389,-46.568148"
    }

    var popupTxt = "<strong>Edificação: </strong><br>" + index["Lote"] +
      "<br><strong>Location: </strong><br>" + index["Situacao"] +
      "<br><strong>Data: </strong><br>" + index["Valor"] +
      "<br><strong>Area: </strong><br>" + index["Area"]

    var popuploc = "";
    if (e.lngLat) {
      popuploc = [e.lngLat.lng, e.lngLat.lat]
    } else {
      popuploc = e;
    }

    /* change to zp-map--info-box--text*/
    $("#map-layer--info-txt").html(popupTxt)
    /*change to zp-map--info-box*/
    $("#mm-info-box").show();
    $(".map-layer--info-close").click(function () {
      map.removeLayer('selected')
      //map.removeSource('selected');
    });

    $("#map-loading").hide();

    map.addLayer({
      "id": "selected",
      "type": "line",
      "source": "LotesSource",
      "paint": {
        "line-color": "firebrick",
        "line-width": 6
      },
      'filter': ['==', 'Lote', uid2]
    });

    searchFound = false;
  }
}

/*
* search
*/

function searchInit(layer, el) {

  /*initiate search function - see simple jekyll search assets/jsonsearch/search.js*/

  //console.log('checking if search json is ready');
  var LotesGeoid = 0;

  LotesSearch = LotesData.features.map(function (f) {
    var centroid = turf.center(f);
    var arr = []
    arr.push(Number(centroid.geometry.coordinates[0]))
    arr.push(Number(centroid.geometry.coordinates[1]))
    f.properties.i = LotesGeoid;
    LotesGeoid = LotesGeoid + 1;
    f.properties.centroid = arr;
    var lotesOk = f.properties.Lote;
    f.properties.Lote = lotesOk;
    return f.properties
  });

  if (LotesSearch != "") {

    //console.log('initiating search');

    SimpleJekyllSearch({
      searchInput: document.getElementById('map-controls--search-input'),
      resultsContainer: document.getElementById('map-controls--search-results'),
      json: LotesSearch,
      searchResultTemplate: '<a href="#" id="{Lote}" data="{centroid}"> \<li>Lote: {Lote} - Area: {Area}<br>Situação: {Situacao}</li><hr></a>',
      noResultsText: 'Não foi encontrado nenhum resultado!',
      limit: 5,
      fuzzy: false,
      exclude: ['exclude']
    });

    /* results click function */

    $("#map-controls--search-results").on('click', 'a', function (event) {
      tries = 0;
      popup.remove();
      searchFound = true;
      searchResultId = $(this).attr("id");
      searchsearchResultString = searchResultId.toString();
      var xyString = $(this).attr("data");
      var xy = xyString.split(",");
      var point = [Number(xy[0]), Number(xy[1])];
      map.flyTo({ center: point, zoom: 19.49 }); //map.getZoom()
      flying = false;
      var mdlLayout = document.querySelector('.mdl-layout');
      if (document.body.querySelector('.mdl-layout__obfuscator.is-visible')) {
        mdlLayout.MaterialLayout.toggleDrawer();
      }

      var queQueryInterval = setInterval(queQuery, 300);

      function queQuery() {
        if (flying === false) {
          //console.log('getting info from search');
          getInfo(point, Number(searchResultId), true);
          clearInterval(queQueryInterval)
        }
      }

      event.preventDefault();
    });
  } else {
    setTimeout(searchInit, 500);
  }
}

/*
* end search function
*/

/* SCREENSHOT FUNCTION */

$(".mm-modal--map-info-open").click(function () {
  var blobUrl = map.getCanvas().toDataURL();
  var imgcard = $("#mm-modal--map-info .mdl-card");
  var background = "url('" + blobUrl + "')";
  //imgcard.css("background","url('" + blobUrl + "')");
  $("#map-image").html('<img src="data:image/jpg;' + blobUrl + '" width="100%"></img>');
  
  var imgname = 'sos_brumadinho';
  $("#map-image-href").html('<a href="data:image/png;' + blobUrl + '" download="' + imgname + '.png"><i class="material-icons">file_download</i> Download Image</a>')
  $("#mm-modal--map-info").fadeIn();
});

/* end image function */

$(".mm-modal").on('click', '.mm-modal--close', function () {
  $(".mm-modal").fadeOut();
});
$(".mm-modal--open").click(function () {
  $("#mm-modal-info").fadeIn();
});
$(".map-layer--info-close").click(function () {
  $("#mm-info-box").hide();
});
var modalshown = localStorage.getItem('modalshown');
if (modalshown == null) {
  localStorage.setItem('modalshown', 1);
  $("#mm-modal-info").fadeIn("slow");
}
$("#mapLayerInfoClose").click(function () {
  $("#mapLayerInfoWrapper").hide();
});
